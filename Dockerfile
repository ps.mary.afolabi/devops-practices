FROM openjdk:8-jdk-alpine
RUN adduser -D mary
USER mary
WORKDIR /assignment
COPY target/*.jar  /assignment/
EXPOSE 8091
ENV PROFILE=${PROFILE}
ENTRYPOINT java -jar -Dspring.profiles.active=${PROFILE} /assignment/*.jar