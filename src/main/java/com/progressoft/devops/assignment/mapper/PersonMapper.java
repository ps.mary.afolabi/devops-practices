package com.progressoft.devops.assignment.mapper;

import com.progressoft.devops.assignment.dto.Person;
import com.progressoft.devops.assignment.model.PersonEntity;

public class PersonMapper {

    public static PersonEntity toEntity(Person person){
        return new PersonEntity(person.getId(), person.getName(), person.getAge());
    }

    public static PersonEntity toEntity(Person person, Long id){
        return new PersonEntity(id, person.getName(), person.getAge());
    }

    public static Person fromEntity(PersonEntity entity){
        return new Person(entity.getId(), entity.getName(), entity.getAge());
    }
}
